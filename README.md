# 1percent

1percent.us -- python flask dash webapps for HonHedgeFund-rwxrwxr-x 1 hon hon  185 Mar  8 09:57 12data.py

Initialized with ...

Dash interactive table prototypes under ./wapps

---

NGINX Ubuntu 18:
https://www.digitalocean.com/community/tutorials/how-to-serve-flask-applications-with-gunicorn-and-nginx-on-ubuntu-18-04

nginx https gunicorn systemd:
sudo add-apt-repository ppa:certbot/certbot
sudo apt install python-certbot-nginx
sudo certbot --nginx -d 1percent.us -d www.1percent.us

---

prototypes from Jun 2020:

12data.sh and 12data.py -- https://pypi.org/project/twelvedata/ ...  pip install twelvedata[pandas,matplotlib,plotly]

fmpcloud.py

ondemand_client.py -- barcharts

rapid_api.py

tda.py

yfinance_rt.py

yfi.py

---

and Submodules:

git submodule add https://github.com/twelvedata/twelvedata-python

git commit -m '12data repo submodule' .

git submodule add https://github.com/DataMining4Finance/yfinance

git commit -m 'yfinance repo submodule' .


