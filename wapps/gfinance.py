#!/usr/bin/env python

from googlefinance import getQuotes
import json, time
import os, sys

def get_quote(symbol):
  q = getQuotes(symbol) #; print json.dumps(q, indent=2)
  return q


if __name__ == '__main__':
  symbol=sys.argv[1]
  q = get_quote(symbol)
  print(q)

