#!/usr/bin/env python
from flask import Flask
import pandas as pd
import dash_table
import dash

srv = Flask(__name__)

@srv.route('/')
def hello_world():
  return 'Hello World!'

DataFrame = pd.read_csv("https://raw.githubusercontent.com/plotly/datasets/master/solar.csv")
data_dict = DataFrame.to_dict('records')
cols = [{'id': c, 'name': c} for c in DataFrame.columns]
style_dict = {'height': '300px', 'overflowY': 'auto'}

dapp = dash.Dash(__name__, server=srv, routes_pathname_prefix='/dash/')
dapp.layout = dash_table.DataTable(id = "table", data=data_dict, columns=cols, page_action='none',style_table=style_dict)

if __name__ == '__main__':
# dapp.run_server()
# dapp.run_server(host='74.91.21.42', port=8000, debug=True)
  dapp.run_server(host='0.0.0.0', port=8888, debug=True)

