#!/usr/bin/env python
import collections
import dash
import pandas as pd

from dash.dependencies import Output, Input
from dash.exceptions import PreventUpdate

import dash_html_components as html
import dash_core_components as dcc
import dash_table

dapp = dash.Dash(__name__)

df = pd.read_csv('https://raw.githubusercontent.com/'+'plotly/datasets/master/gapminderDataFiveYear.csv')

countries = set(df['country'])

dapp.layout = html.Div([
  dcc.Store(id='memory-output'),
  dcc.Dropdown(id='memory-countries', options=[
    {'value': x, 'label': x} for x in countries
  ], multi=True, value=['Canada', 'United States']),
  dcc.Dropdown(id='memory-field', options=[
    {'value': 'lifeExp', 'label': 'Life expectancy'},
    {'value': 'gdpPercap', 'label': 'GDP per capita'},
  ], value='lifeExp'),
  html.Div([
    dcc.Graph(id='memory-graph'),
    dash_table.DataTable(
      id='memory-table',
      columns=[{'name': i, 'id': i} for i in df.columns],
      editable=True
    ),
  ])
])


@dapp.callback(Output('memory-output', 'data'),[Input('memory-countries', 'value')])
def filter_countries(countries_selected):
  if not countries_selected:
    # Return all the rows on initial load/no country selected.
    return df.to_dict('rows')

  filtered = df.query('country in @countries_selected')

  return filtered.to_dict('rows')


@dapp.callback(Output('memory-table', 'data'),[Input('memory-output', 'data')])
def on_data_set_table(data):
  if data is None:
    raise PreventUpdate

  return data

if __name__ == '__main__':
# dapp.run_server()
  dapp.run_server(host='0.0.0.0', port=8888, debug=True)

